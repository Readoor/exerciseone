//
//  excercise_oneTests.swift
//  excercise-oneTests
//
//  Created by Evgeny Ratnikov on 25/12/2017.
//  Copyright © 2017 MySelf. All rights reserved.
//

import XCTest
@testable import excercise_one

class ExcerciseOneTests: XCTestCase {
    
    var userData:UsersData!
    var excerciseOneVC:ExcerciseOne!
    
    var firstTestDictionaryUsersEmails : [String: Set<String>] = [String: Set<String>]()
    var secondTestDictionaryUsersEmails : [String: Set<String>] = [String: Set<String>]()
    var thirdTestDictionaryUsersEmails : [String: Set<String>] = [String: Set<String>]()
    
    var expectedResultFirstInput : [String: Set<String>]!
    var expectedResultSecondInput : [String: Set<String>]!
    var expectedResultThirdInput : [String: Set<String>]!
    
    override func setUp() {
        super.setUp()
        userData = UsersData()
        excerciseOneVC = ExcerciseOne()

        firstTestDictionaryUsersEmails = userData.inputFirstTestDictionary
        secondTestDictionaryUsersEmails = userData.inputSecondTestDictionary
        thirdTestDictionaryUsersEmails = userData.inputThirdTestDictionary
        
        
        expectedResultFirstInput = ["u2": ["j", "n", "u", "d", "1", "8", "m", "3", "e", "s", "g", "l", "y", "5"]]
            
        expectedResultSecondInput = ["u8": ["w", "34", "n", "v", "u", "x", "q", "b", "8", "r", "c", "e", "7", "y", "h", "j", "o", "k", "d", "t", "1", "i", "m", "l", "g", "5"]]
            
        expectedResultThirdInput = ["u5": ["12", "34", "41", "w", "n", "v", "9", "u", "45", "22", "x", "b", "8", "r", "6", "c", "e", "7", "44", "h", "88", "y", "j", "p", "o", "f", "k", "d", "t", "33", "2", "1", "a", "i", "m", "3", "4", "s", "l", "g", "0", "10", "5"]]
    }
    
    override func tearDown() {
        userData = nil
        excerciseOneVC = nil
        super.tearDown()
    }
    
    func testAlgorithmWithFirstInput(){
        
        let resultUnionUsersFirst = excerciseOneVC.unionUsers(firstTestDictionaryUsersEmails)
        
        XCTAssertNotNil(resultUnionUsersFirst)
        
        XCTAssertEqual(resultUnionUsersFirst, expectedResultFirstInput)
        
        XCTAssertFalse(resultUnionUsersFirst != expectedResultFirstInput)
    }
    
    func testAlgorithmWithSecondInput(){
        
        let resultUnionUsersSecond = excerciseOneVC.unionUsers(secondTestDictionaryUsersEmails)
        
        XCTAssertNotNil(resultUnionUsersSecond)
        
        XCTAssertEqual(resultUnionUsersSecond, expectedResultSecondInput)
        
        XCTAssertFalse(resultUnionUsersSecond != expectedResultSecondInput)
    }
    
    func testAlgorithmWithThirdInput(){
        
        let resultUnionUsersThird = excerciseOneVC.unionUsers(thirdTestDictionaryUsersEmails)
        
        XCTAssertNotNil(resultUnionUsersThird)
        
        XCTAssertEqual(resultUnionUsersThird, expectedResultThirdInput)
        
        XCTAssertFalse(resultUnionUsersThird != expectedResultThirdInput)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            
              excerciseOneVC.unionUsers(firstTestDictionaryUsersEmails)

//            excerciseOneVC.unionUsers(secondTestDictionaryUsersEmails)

//            excerciseOneVC.unionUsers(thirdTestDictionaryUsersEmails)

        }
    }
    
}
