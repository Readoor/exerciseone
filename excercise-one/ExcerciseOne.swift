//
//  Created by Evgeny Ratnikov on 21/12/2017.
//  Copyright © 2017 MySelf. All rights reserved.
//

import UIKit

class ExcerciseOne {
    
    //Алгоритм обьединения пользователей по одинаковым элементам в множестве, где emailsByUsers - словарь неупорядочных множеств email-aдресов с ключами именами пользователей)
    func unionUsers (_ emailsByUsers : [String: Set<String>]) -> [String: Set<String>] {
        
        var usersByUsers : [String: String] = [String: String]() // Словарь user -> mainUser
        
        prepareMainUsers(emailsByUsers, &usersByUsers)
        
        return unionUsersByMainUsers(emailsByUsers, usersByUsers)
        
    }
    
    func prepareMainUsers( _ emailsByUsers : [String: Set<String>], _ usersByUsers : inout [String: String] ) {
        
        var userByEmail : [String: String] = [String: String]() // Кеш - словарь email -> user для нахождения mainUser
        var usersToUnion : Set<String> = Set<String>() // Пользователи с одинаковыми email - адресами
        
        for (user, emails) in emailsByUsers {
            for email in emails {
                // Если находим user, у которого уже ранее встречался такой же email, помещаем его в массив без дублирования
                if let existingUser : String = userByEmail[email] {
                    usersToUnion.insert(existingUser)
                } else { // Иначе просто записываем user и емейл в кеш userByEmail
                    userByEmail[email] = user
                }
            }
            
            if usersToUnion.isEmpty {
                usersByUsers[user] = user
                
            } else {
                // Инициализируем mainUser, присваивая ему значение firstUser в userToUnion, или mainUser первого пользователя, если он существует.
                var mainUser : String = usersToUnion.first!
                if let newMainUser = usersByUsers[mainUser] {
                    mainUser = newMainUser
                }
                // Добавляем user, с которым совпали email на этой итерации
                usersToUnion.insert(user)
                for userToUnion in usersToUnion {
                    if userToUnion != mainUser {
                        // Обновляем в кеше userByEmail принадлежность email к main пользователю
                        for (e, u) in userByEmail {
                            if u == userToUnion {
                                userByEmail[e] = mainUser
                            }
                        }
                        //Обновляем отношения user -> mainUser
                        usersByUsers[userToUnion] = mainUser
                    }
                }
                usersToUnion = []
            }
        }
    }
    
    func unionUsersByMainUsers(_ emailsByUsers:[String: Set<String>], _ usersByUsers:[String: String]) -> [String: Set<String>]{
        
        var unionEmailsByUsers : [String: Set<String>] = [String: Set<String>]() // Выходные данные
        
        //Инициализация user и superMainUser и их объединение, исходя из принадлежности к superMainUser
        for (user, mainUser) in usersByUsers {
            let userEmails = emailsByUsers[user]
            var superMainUser = usersByUsers[mainUser]!
            var oldMainUser : String
            
            //Проверка на вложенность superMainUser
            repeat {
                oldMainUser = superMainUser
                superMainUser = usersByUsers[superMainUser]!
            } while oldMainUser != superMainUser
            //Объединение пользователей
            if let mainEmails = unionEmailsByUsers[superMainUser] {
                unionEmailsByUsers[superMainUser] = mainEmails.union(userEmails!)
            } else {
                unionEmailsByUsers[superMainUser] = userEmails
            }
        }
        
        return unionEmailsByUsers
    }
}

