//
//  Data.swift
//  objectiveFirst
//
//  Created by Evgeny Ratnikov on 22/12/2017.
//  Copyright © 2017 MySelf. All rights reserved.
//

import Foundation

struct UsersData {
    
    var inputFirstTestDictionary : [String: Set<String>] = [
        "u1": ["d", "s", "e"],
        "u2": ["n", "j", "m", "y"],
        "u3": ["u", "3", "g", "5", "8"],
        "u4": ["1", "d","3"],
        "u5": ["e", "j", "l"],
        ]
    var inputSecondTestDictionary  : [String: Set<String>] = [
        "u1": ["d", "v", "e"],
        "u2": ["n", "j", "c", "y"],
        "u3": ["u", "34", "g", "5", "8"],
        "u4": ["1", "c","i"],
        "u5": ["e", "j", "l"],
        "u6": ["h", "7", "m", "i"],
        "u7": ["q", "8", "x", "5"],
        "u8": ["v", "r", "g"],
        "u9": ["l", "t","k"],
        "u10": ["o", "b", "34", "w"],
        ]
    var inputThirdTestDictionary  : [String: Set<String>] = [
        "u1": ["d", "s", "e"],
        "u2": ["n", "j", "m", "y"],
        "u3": ["u", "3", "g", "5", "8"],
        "u4": ["1", "c","3","f"],
        "u5": ["e", "44", "l"],
        "u6": ["h", "7", "m", "i"],
        "u7": ["22", "w", "x", "b"],
        "u8": ["v", "r", "g", "2"],
        "u9": ["l", "33","k", "b"],
        "u10": ["1", "2", "0"],
        "u12": ["34", "c", "v", "t"],
        "u13": ["45", "k", "n"],
        "u14": ["10", "9", "4"],
        "u15": ["a", "b", "c", "d", "e"],
        "u16": ["o", "p", "6"],
        "u17": ["g", "k", "m", "41"],
        "u18": ["l", "f", "h", "34"],
        "u19": ["g", "12", "2", "45"],
        "u20": ["88", "10", "p", "44"],
        ]
}
