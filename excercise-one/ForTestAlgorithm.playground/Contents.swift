import UIKit
import Foundation

///////////////////////////////Данные для алгоритма/////////////////////////////////
var firstDictionaryUsersEmails : [String: Set<String>] = [
    "u1": ["d", "s", "e"],
    "u2": ["n", "j", "m", "y"],
    "u3": ["u", "3", "g", "5", "8"],
    "u4": ["1", "d","3"],
    "u5": ["e", "j", "l"],
]
var secondDictionaryUsersEmails : [String: Set<String>] = [
    "u1": ["d", "v", "e"],
    "u2": ["n", "j", "c", "y"],
    "u3": ["u", "34", "g", "5", "8"],
    "u4": ["1", "c","i"],
    "u5": ["e", "j", "l"],
    "u6": ["h", "7", "m", "i"],
    "u7": ["q", "8", "x", "5"],
    "u8": ["v", "r", "g"],
    "u9": ["l", "t","k"],
    "u10": ["o", "b", "34", "w"],
]
var thirdDictionaryUsersEmails : [String: Set<String>] = [
    "u1": ["d", "s", "e"],
    "u2": ["n", "j", "m", "y"],
    "u3": ["u", "3", "g", "5", "8"],
    "u4": ["1", "c","3","f"],
    "u5": ["e", "44", "l"],
    "u6": ["h", "7", "m", "i"],
    "u7": ["22", "w", "x", "b"],
    "u8": ["v", "r", "g", "2"],
    "u9": ["l", "33","k", "b"],
    "u10": ["1", "2", "0"],
    "u12": ["34", "c", "v", "t"],
    "u13": ["45", "k", "n"],
    "u14": ["10", "9", "4"],
    "u15": ["a", "b", "c", "d", "e"],
    "u16": ["o", "p", "6"],
    "u17": ["g", "k", "m", "41"],
    "u18": ["l", "f", "h", "34"],
    "u19": ["g", "12", "2", "45"],
    "u20": ["88", "10", "p", "44"],
]

////////////////////////////////////////////////////////////////////////////////////////////////////
//Вывод результата в консоль
func printResult(result:[String:Set<String>], nameDictionary: String){
    print(".................................Результат " + nameDictionary + "...............................")
    print("")
    print(result)
    print("")
}
////////////////////////////////////////////////////////////////////////////////////////////////////

//Алгоритм обьединения пользователей по одинаковым элементам в множестве, где emailsByUsers - словарь неупорядочных множеств email-aдресов с ключами именами пользователей)
func unionUsers (_ emailsByUsers : [String: Set<String>]) -> [String: Set<String>] {
    
    var usersByUsers : [String: String] = [String: String]() // Словарь user -> mainUser
    
    prepareMainUsers(emailsByUsers, &usersByUsers)
    
    return unionUsersByMainUsers(emailsByUsers, usersByUsers)
    
}

func prepareMainUsers( _ emailsByUsers : [String: Set<String>], _ usersByUsers : inout [String: String] ) {
    
    var userByEmail : [String: String] = [String: String]() // Кеш - словарь email -> user для нахождения mainUser
    var usersToUnion : Set<String> = Set<String>() // Пользователи с одинаковыми email - адресами
    
    for (user, emails) in emailsByUsers {
        for email in emails {
            // Если находим user, у которого уже ранее встречался такой же email, помещаем его в массив без дублирования
            if let existingUser : String = userByEmail[email] {
                usersToUnion.insert(existingUser)
            } else { // Иначе просто записываем user и емейл в кеш userByEmail
                userByEmail[email] = user
            }
        }
        
        if usersToUnion.isEmpty {
            usersByUsers[user] = user
            
        } else {
            // Инициализируем mainUser, присваивая ему значение firstUser в userToUnion, или mainUser первого пользователя, если он существует.
            var mainUser : String = usersToUnion.first!
            if let newMainUser = usersByUsers[mainUser] {
                mainUser = newMainUser
            }
            // Добавляем user, с которым совпали email на этой итерации
            usersToUnion.insert(user)
            for userToUnion in usersToUnion {
                if userToUnion != mainUser {
                    // Обновляем в кеше userByEmail принадлежность email к main пользователю
                    for (e, u) in userByEmail {
                        if u == userToUnion {
                            userByEmail[e] = mainUser
                        }
                    }
                    //Обновляем отношения user -> mainUser
                    usersByUsers[userToUnion] = mainUser
                }
            }
            usersToUnion = []
        }
    }
}

func unionUsersByMainUsers(_ emailsByUsers:[String: Set<String>], _ usersByUsers:[String: String]) -> [String: Set<String>]{
    
    var unionEmailsByUsers : [String: Set<String>] = [String: Set<String>]() // Выходные данные
    
    //Инициализация user и superMainUser и их объединение, исходя из принадлежности к superMainUser
    for (user, mainUser) in usersByUsers {
        let userEmails = emailsByUsers[user]
        var superMainUser = usersByUsers[mainUser]!
        var oldMainUser : String
        
        //Проверка на вложенность superMainUser
        repeat {
            oldMainUser = superMainUser
            superMainUser = usersByUsers[superMainUser]!
        } while oldMainUser != superMainUser
        //Объединение пользователей
        if let mainEmails = unionEmailsByUsers[superMainUser] {
            unionEmailsByUsers[superMainUser] = mainEmails.union(userEmails!)
        } else {
            unionEmailsByUsers[superMainUser] = userEmails
        }
    }
    
    return unionEmailsByUsers
}

// Запуск алгоритма и вывод результата в консоль

var result1 = unionUsers(firstDictionaryUsersEmails)
printResult(result: result1, nameDictionary: "firstDictionaryUsersEmails")

var result2 = unionUsers(secondDictionaryUsersEmails)
printResult(result: result2, nameDictionary: "firstDictionaryUsersEmails")
//
var result3 = unionUsers(thirdDictionaryUsersEmails)
printResult(result: result3, nameDictionary: "firstDictionaryUsersEmails")
